<?php
$pure_numeric = [1,2,3,4];  // not used
$bit = [0,1];
$tinyint_signed = [-128,127];
$tinyint_unsigned = [0,255];
$mediumint_signed = [-8388608,8388607];
$mediumint_unsigned = [0,16777215];
$int_signed = [-2147483648,2147483647];
$int_unsigned = [0,4294967295];

// BIGINT ESTO VA A SER DAR PROBLEMA!Esta es una var_dump para los datos de int_*, por dentro php los pasa a float.
// $bigint_signed =[-9223372036854775808,9223372036854775807];
// $bigint_unsigned = [0,18446744073709551615];

  // ["bigint_signed"]=>
  // array(2) {
  //   [0]=>
  //   float(-9.2233720368548E+18)
  //   [1]=>
  //   int(9223372036854775807)
  // }
  // ["bigint_unsigned"]=>
  // array(2) {
  //   [0]=>
  //   int(0)
  //   [1]=>
  //   float(1.844674407371E+19)
  // }
