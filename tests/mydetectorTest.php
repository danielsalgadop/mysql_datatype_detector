<?php
use PHPUnit\Framework\TestCase;
require('mydetector.php');
include('tests/models/number.php');
include('tests/models/string.php');

// Expected => Actual (achieved)
class intTest extends TestCase
{
    // (TODO) mover este metodo a usage test
    public function testNoData()
    {
        $md = new MyDetector([]);
        $this->assertEquals($md->result, "ERROR: NO DATA");
    }

    // public function testBIT()
    // {
    //     global $bit;
    //     $md = new MyDetector($bit);
    //     var_dump($md->result);
    //     $this->assertArraySubset( ['type'=>"BIT" , 'has_sign' => 'not_set' , 'has_null' => 'not_set'] , $md->result );
    // }
    public function testTINYINT()
    {
        global $tinyint_signed;
        global $tinyint_unsigned;
        $md = new MyDetector($tinyint_signed);
        $this->assertArraySubset( ['type'=>"TINYINT" , 'has_sign' => true ] , $md->result );

        $md = new MyDetector($tinyint_unsigned);
        $this->assertArraySubset( ['type'=>"TINYINT" , 'has_sign' => false ] , $md->result );
    }
    public function testMEDIUMINT()
    {
        global $mediumint_signed;
        global $mediumint_unsigned;
        $md = new MyDetector($mediumint_signed);
        $this->assertArraySubset(['type' => 'MEDIUMINT' , 'has_sign' => true ],$md->result);

        $md = new MyDetector($mediumint_unsigned);
        $this->assertArraySubset(['type' => 'MEDIUMINT' , 'has_sign' => false ],$md->result);
    }
    public function testINT()
    {
        global $int_signed;
        global $int_unsigned;
        $md = new MyDetector($int_signed);
        $this->assertArraySubset(['type' => 'INT' , 'has_sign' => true ],$md->result);

        $md = new MyDetector($int_unsigned);
        $this->assertArraySubset(['type' => 'INT' , 'has_sign' => false ],$md->result);
    }
    // TODO:
    // public function testBIGINT(){}  // OJO CON ESTE mirar models/data


    // STRING TESTS
    public function testBasicString()
    {
        global $string;
        $md = new MyDetector($string);
        // print_r($md);
        $this->assertArraySubset( ['type'=>"CHAR" , 'has_sign' => false, 'max' => 2 ] , $md->result );
    }
    public function testBasicString2()
    {
        global $string2;
        $md = new MyDetector($string2);
        print_r($md);
        $this->assertArraySubset( ['type'=>"CHAR" , 'has_sign' => false, 'max' => 1325 ] , $md->result );
    }
}