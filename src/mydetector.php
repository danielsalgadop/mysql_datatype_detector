<?php
// https://dev.mysql.com/doc/refman/5.7/en/data-types.html
// Mysql has more "MAIN" datatypes than this. Este script soportará estas
// 	- NUMERIC:     [1,2,3,-1,-2]
// 	- DATE			[2017-02-12, 2017]
// 	- STRING		["Et voluptate et enim tempor."]
// 	- JSON			[{"a":"b"}]

//

// modelo de datos que define INT
$int = [
	'BIT' => [0,1],
	'TINYINT_SIGNED' => [-128,127],
	'TINYINT_UNSIGNED' => [0,255],
	'MEDIUMINT_SIGNED' => [-8388608,8388607],
	'MEDIUMINT_UNSIGNED' => [0,16777215],
	'INT_SIGNED' => [-2147483648,2147483647],
	'INT_UNSIGNED' => [0,4294967295],
	'BIGINT_SIGNED' =>[-9223372036854775808,9223372036854775807],
	'BIGINT_UNSIGNED' => [0,18446744073709551615],
];

/**
* 2 walkthrought
 * 	1. Determinar el Main Tipo
 * 	2. Determinar el Subtipo y length
*/
class MyDetector
{
	private $type 			= NULL;
	private $has_null 		= NULL;
	private $has_sign 		= NULL;
	// private $length 		= 0;
	private $data 			= [];
	private $max 		= 0;   // in CHAR this is length
	private $min 		= 0;
	public $result;
	public $sql_sentence = "";
	// private $main_types = ["numeric","string","time"];

	function __construct($a_data)
	{
		if(!$a_data)
		{
			$this->result = "ERROR: NO DATA";
		}
		else
		{
			$this->data = $a_data;
			$this->detMainType();    // determina main type
			$this->detMinMax();  // determina Min y Max
			$this->detSubType();    // determina subtipo
			// $this->mySqlBuilder();
			$this->output();
		}
	}

	// will built apropiated atribute definition
	private function output(){
		$this->result = ['type'=>$this->type, 'has_sign'=>$this->has_sign,'has_null:'=>$this->has_null,'max'=>$this->max];
	}

	// aqui hay diferencias entre MySQL y PHP, ya que MySQL NO diferencia 0 o '' de false. En cambio MySQL sí.
	private function isNull($value){
		return ($value || $value == 0 || $value == '') ? false : true;
	}
	private function isNumeric($value){
		return is_numeric($value);
	}
	private function isChar($value){
		return is_string($value);
	}


	private function setNumeric(){
		if($this->type == NULL){ // only if unseted
			$this->type= "INT";
		}
	}

	private function setChar(){
		$this->type = "CHAR";
	}
	// will fill $private properties
	private function detMainType(){
		// detect type
        foreach ($this->data as $value) {
            if(!$this->IsNull($value)){
                if($this->isNumeric($value)){
                    $this->setNumeric();
                }
                else if($this->isChar($value)){
                    $this->setChar();
                }
            }
            else{  // is NULL
                $this->has_null = true;
            }
        }
    }

    private function detMinMax(){
		foreach ($this->data as $value) {
            if($this->type == "CHAR"){
                $value = strlen($value);
            }
            $this->fillMaxMin($value);
        }
    }

	// diferenciacion "fina", de numerico a =int, tinyint, bigint etc...
	private function detSubType(){
				// print "\n";
				// print ">>>>>>>>>>>".$this->min."\n";
				// print ">>>>>>>>>>>".$this->max."\n";
		if($this->type == "INT"){
			global $int;
			foreach ($int as $datatype => $a_min_max) {
				if($this->min >= $a_min_max[0] && $this->max <= $a_min_max[1]){
					// parse datatype
					if(strpos($datatype,'_') !== false){
						$exploded_datatype = explode('_',$datatype);
						// set has_sign
						if($exploded_datatype[1] == "SIGNED")
							$this->has_sign = true;
						else
							$this->has_sign = false;
						$datatype = $exploded_datatype[0];
					}
					$this->type=$datatype;
					break;
				}
			}
		}
	}

	// will build SQL sentence
	// private function mySqlBuilder(){
	// 	$this->sql_sentence = "`name` INT";
	// }

	private function fillMaxMin($value){
		if($this->max < $value){
			$this->max = $value;
		}
		if($this->min > $value){
			$this->min = $value;
		}
	}
}

// $myDetector = new MyDetector($pure_numeric);